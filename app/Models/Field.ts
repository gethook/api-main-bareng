import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

import Booking from 'App/Models/Booking'
import Venue from 'App/Models/Venue'

export default class Field extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public type: 'futsal' | 'mini soccer' | 'basketball'

  @column()
  public venue_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public venueId: number

  @hasMany(() => Booking)
  public bookings: HasMany<typeof Booking>

  @belongsTo(() => Venue)
  public venue: BelongsTo<typeof Venue>
}
