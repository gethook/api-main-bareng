import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'

import Field from 'App/Models/Field'
import Booking from 'App/Models/Booking'
import User from 'App/Models/User'

export default class BookingsController {

	/**
	 * @swagger
	 * /bookings:
	 *   get:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Bookings
	 *     summary: Get all bookings
	 *     responses:
	 *       200:
	 *         description: berhasil get all bookings
	 *       400:
	 *         description: Gagal query data
	 *       401:
	 *         description: token tidak valid
	 */
	 public async index({ response} : HttpContextContract) {
		// const field = await Field.findOrFail(params.id)
		// const bookings = await Field.query().preload('bookings').preload('venue').where('id', params.id).first()
		const bookings = await Booking.query().preload('field', (fieldQuery) => {
			fieldQuery.preload('venue')
		})
		return response.ok({message: 'berhasil get data bookings', data: bookings})
	}

	/**
	 * @swagger
	 * /fields/{field_id}/bookings:
	 *   post:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Bookings
	 *     summary: Create new booking
	 *     parameters:
	 *       - name: field_id
	 *         in: path
	 *         description: field id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     requestBody:
	 *       description: play date start, play date end
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               play_date_start:
	 *                 type: string
	 *               play_date_end:
	 *                 type: string
	 *             required: 
	 *               - play_date_start
	 *               - play_date_end
	 *     responses:
	 *       200:
	 *         description: berhasil create booking
	 *       400:
	 *         description: Gagal create booking
	 *       401:
	 *         description: token tidak valid
	 */
	 public async store({ request, params, auth, response } : HttpContextContract) {
		try {
			const payload = await request.validate(CreateBookingValidator);

			const oneHourAfterStart = payload.play_date_start.plus({hours: 1})
			if (payload.play_date_end < oneHourAfterStart) {
				return response.badRequest({message: 'Booking minimal 1 jam'})
			}
			const field = await Field.findOrFail(params.field_id)
			const booked = await field.related('bookings').query()
				.where((query) => {
					query
						.where('play_date_start', '<=', payload.play_date_start.toSQL())
						.where('play_date_end', '>=', payload.play_date_start.toSQL())
				})
				.orWhere((query) => {
					query
					.where('play_date_start', '<=', payload.play_date_end.toSQL())
					.where('play_date_end', '>=', payload.play_date_end.toSQL())
				})
				.first()
			if (booked) {
				return response.badRequest({
					message: `Field ${field.name} sudah dibooking pada jam tersebut, silakan pilih waktu yang lain`,
					data: {
						field_name: field.name,
						date: booked.playDateStart.setLocale('id').toFormat('dd LLLL yyyy'),
						start: booked.playDateStart.setLocale('id').toFormat('hh:mm:ss'),
						end: booked.playDateEnd.setLocale('id').toFormat('hh:mm:ss')
					}
				})
			}
			// response.send(booked)
			// return console.log(booked)
			const booking = await field.related('bookings').create({
				userId: auth.user?.id,
				playDateStart: payload.play_date_start,
				playDateEnd: payload.play_date_end
			})
			response.created({message: `berhasil booking field ${field.name}`, data: {booking_id: booking.id}});
		} catch (error) {
			let message = error.message
			if (error.messages !== undefined)
				message = error.messages
			response.badRequest({message});
		}
	}

	/**
	 * @swagger
	 * /bookings/{id}:
	 *   get:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Bookings
	 *     summary: Get booking by id
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: booking id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: berhasil get booking
	 *       400:
	 *         description: Gagal query data
	 *       401:
	 *         description: token tidak valid
	 */
	 public async show({ params, response } : HttpContextContract) {
		try {
			let booking = await Booking
				.query()
				.preload('players')
				.preload('field', (fieldQuery) => {
					fieldQuery.preload('venue')
				})
				.preload('bookedBy')
				.withCount('players')
				.where('id', params.id)
				.firstOrFail()
			const players_count = booking.$extras.players_count
			// const output = {...orig, booking.$preload, players_count: booking.$extras.users_count}
			return response.ok({ message: 'berhasil get data booking by id', data: {booking, players_count}})			
		} catch (error) {
			return response.badRequest({message: error.message})
		}
	}

	/**
	 * @swagger
	 * /bookings/{id}/join:
	 *   put:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Bookings
	 *     summary: Join booking
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: booking id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: berhasil join booking
	 *       400:
	 *         description: Gagal query data
	 *       401:
	 *         description: token tidak valid
	 */
	 public async join({ params, auth, response } : HttpContextContract) {
		try {
			const booking = await Booking.findOrFail(params.id)
			const user_id = auth.user?.id || 0;
			const userJoined = await booking.related('players').query().wherePivot('user_id', user_id).first()
			if (userJoined) {
				return response.badRequest({message: 'Anda sudah join pada jadwal ini, silakan pilih jadwal yang lain.'})
			}
			await booking.related('players').attach([user_id])
	
			response.ok({message: 'berhasil join booking'})				
		} catch (error) {
			return response.badRequest({message: error.message})
		}
	}

	/**
	 * @swagger
	 * /bookings/{id}/unjoin:
	 *   put:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Bookings
	 *     summary: Unjoin booking
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: booking id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: berhasil unjoin booking
	 *       400:
	 *         description: Gagal unjoin
	 *       401:
	 *         description: token tidak valid
	 */
	 public async unjoin({params, auth, response}: HttpContextContract) {
		try {
			const user_id = auth.user?.id || 0;
			// const booking = await Booking.query().where('id', params.id).where('user_id', user_id).firstOrFail()
			const booking = await Booking.findOrFail(params.id)
			const schedule = await Booking.query().preload('players', (playersQuery) => {
				playersQuery.where('user_id', user_id)
			}).where('id', params.id).firstOrFail()
			if (schedule.players.length == 0) {
				return response.badRequest({message: 'Anda tidak terdaftar pada jadwal ini'})
			}
			// return response.send({booking, schedule})
			booking.related('players').detach([user_id])
			response.ok({message: 'Berhasil unjoin booking pada jadwal ' + schedule.playDateStart.toFormat('dd LLLL yyyy HH:mm:ss')}) 
		} catch (error) {
			return response.badRequest({message: error.message})			
		}
	}

	/**
	 * @swagger
	 * /schedules:
	 *   get:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Bookings
	 *     summary: User schedules
	 *     responses:
	 *       200:
	 *         description: berhasil get user schedules
	 *       400:
	 *         description: Gagal get schedules
	 *       401:
	 *         description: token tidak valid
	 */
	 public async getUserSchedule({auth, response}: HttpContextContract) {
		try {
			const user_id = auth.user?.id || 0;
			const user = await User.query().preload('bookings', bookingsQuery => {
				bookingsQuery.preload('field', fieldQuery => {
					fieldQuery.preload('venue')
				})
			}).withCount('bookings').where('id', user_id).firstOrFail()
			const schedules = user.bookings
			const schedules_count = user.$extras.bookings_count
			console.log(user)
			return response.ok({message: 'Berhasil mendapatkan schedules', data: {schedules_count, schedules}})
		} catch (error) {
			console.log(error)
			return response.badRequest()
		}
	}
}
