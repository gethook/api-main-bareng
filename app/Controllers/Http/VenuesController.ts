import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Venue from 'App/Models/Venue'
import { DateTime } from 'luxon'

export default class VenuesController {
	/**
	 * @swagger
	 * /venues:
	 *   get:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Venues
	 *     summary: Get all venues
	 *     responses:
	 *       200:
	 *         description: berhasil get all venues
	 *       400:
	 *         description: Gagal query data
	 *       401:
	 *         description: token tidak valid
	 */
	public async index({ response } : HttpContextContract) {
		try {
			// Menggunakan Query Builder
			// let venues = await Database.from('venues').select('*')

			// Menggunakan Model (ORM)
			// let venues = await Venue.all()

			// ORM hasMany
			let venues = await Venue.query().preload('fields')
			response.send({message: 'Success', data: venues})
		} catch (err) {
			response.badRequest({message: err.message})
		}
	}

	/**
	 * @swagger
	 * /venues:
	 *   post:
	 *     security:
	 *       - bearerAuth: [admin]
	 *     tags:
	 *       - Venues
	 *     summary: Create venue (need 'admin' role)
	 *     requestBody:
	 *       description: name, address, phone
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               name:
	 *                 type: string
	 *               address:
	 *                 type: string
	 *               phone:
	 *                 type: number
	 *             required: 
	 *               - name
	 *               - address
	 *               - phone
	 *     responses:
	 *       200:
	 *         description: Berhasil menambah venue
	 *       400:
	 *         description: Gagal menambah venue
	 *       401:
	 *         description: Token tidak valid/tidak memiliki hak akses
	 */
	 public async store({ bouncer, request, response, auth } : HttpContextContract) {
		try {
			await bouncer.authorize('createVenue')
			const venueSchema = schema.create({
				name: schema.string({trim: true}, [
					rules.alpha({ allow: ['space'] }),
					rules.minLength(5)
				]),
				address: schema.string({trim: true}, [
					rules.alpha({ allow: ['space'] }),
					rules.minLength(5)
				]),
				phone: schema.string({}, [
					rules.mobile(),
					rules.minLength(5)
				])
			});	
			await request.validate({schema: venueSchema});
			// Query Builder
			// let newVenue = await Database.table('venues').returning('id', 'name').insert({
			// 	name: request.input('name'),
			// 	address: request.input('address'),
			// 	phone: request.input('phone')
			// });

			// Model ORM
			let newVenue = new Venue();
			if (auth.user) {
				newVenue.name = request.input('name');
				newVenue.address = request.input('address');
				newVenue.phone = request.input('phone');
				newVenue.user_id = auth.user?.id;					

				await newVenue.save();
			}

			response.created({message: 'Success', data: {id: newVenue['id']}});
		} catch (error) {
			const message = 'messages' in error ? error.messages : error.message
			if (error.code == 'E_AUTHORIZATION_FAILURE') {
				return response.status(401).send({message})
			}
			response.badRequest({message});
		}
		
	}

	/**
	 * @swagger
	 * /venues/{id}:
	 *   get:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Venues
	 *     summary: Get venue by id
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: venue id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: berhasil
	 *       400:
	 *         description: Gagal update data
	 *       401:
	 *         description: token tidak valid
	 */
	 public async show({ request, response } : HttpContextContract) {
		try {
			const today = DateTime.now().startOf('day').toSQL()
			const tomorrow = DateTime.now().plus({days: 1}).endOf('day').toSQL()
			// Query Builder
			// let venue = await Database.from('venues').select('*').where('id', request.params().id).first()

			// Model ORM
			// let venue = await Venue.findOrFail(request.params().id);

			// ORM relationship
			let venue = await Venue.query().preload('fields', (fieldsQuery) => {
				fieldsQuery.preload('bookings', (bookingsQuery) => {
					bookingsQuery.where('play_date_start', '>=', today)
					bookingsQuery.where('play_date_end', '<=', tomorrow)
				})
			}).where('id', request.params().id).firstOrFail();

			response.send({message: 'berhasil get data venue by id', data: venue})
		} catch (err) {
			let msg = err.message
			// if ('code' in err) {
			// 	msg = err.code
			// }
			response.badRequest({message: msg})
			// console.log(err)
			// const status = err.status | 404
			// const {message} = err
			// response.status(status).send({message})
		}
	}

	/**
	 * @swagger
	 * /venues/{id}:
	 *   put:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Venues
	 *     summary: Update venue by id (need 'admin' role)
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: venue id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     requestBody:
	 *       description: name, address, phone
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               name:
	 *                 type: string
	 *               address:
	 *                 type: string
	 *               phone:
	 *                 type: number
	 *     responses:
	 *       200:
	 *         description: berhasil update venue
	 *       400:
	 *         description: Gagal update venue
	 *       401:
	 *         description: token tidak valid/tidak punya hak akses
	 */
	 public async update({ bouncer, request, response } : HttpContextContract) {
		const venueSchema = schema.create({
			name: schema.string.optional({trim: true}, [
				rules.alpha({ allow: ['space'] }),
				rules.minLength(5)
			]),
			address: schema.string.optional({trim: true}, [
				rules.alpha({ allow: ['space'] }),
				rules.minLength(5)
			]),
			phone: schema.string.optional({}, [
				rules.mobile(),
				rules.minLength(5)
			])
		});

		try {
			await bouncer.authorize('updateVenue')
			await request.validate({schema: venueSchema})
			const name : string = request.input('name')
			const address : string = request.input('address')
			const phone : string = request.input('phone')
			// QB
			// const affectedRows = await Database
			// 	.from('venues')
			// 	.where('id', request.params().id)
			// 	.update({name, address, phone})

			// ORM
			let venue = await Venue.findOrFail(request.params().id)
			venue.name = name;
			venue.address = address;
			venue.phone = phone;
			await venue.save();

			response.send({message: 'Success', data: venue.id})
		} catch (err) {
			// let msg = err.message
			// if ('code' in err) {
			// 	msg = err.code
			// }
			response.badRequest({message: err.message})
			console.log(err)
		}
	}

	/**
	 * @swagger
	 * /venues/{id}:
	 *   delete:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Venues
	 *     summary: Delete venue by id (need 'admin' role)
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: venue id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: berhasil hapus venue
	 *       400:
	 *         description: Gagal hapus venue
	 *       401:
	 *         description: token tidak valid/tidak punya hak akses
	 */
	public async destroy({ bouncer, request, response } : HttpContextContract) {
		try {
			// const deleted = await Database
			// 	.from('venues')
			// 	.where('id', request.params().id)
			// 	.delete()
			// const msg = deleted ? 'Success' : 'Tidak ada data yang dihapus'

			// ORM
			await bouncer.authorize('deleteVenue')
			let venue = await Venue.findOrFail(request.params().id)
			await venue.delete()
			response.send({message: 'Success'})
		} catch (err) {
			let msg = err.message
			// if ('code' in err) {
			// 	msg = err.code
			// }
			response.badRequest({message: msg})
			// console.log(err)
		}
	}
}
