import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

enum FieldTypes {
	FUTSAL = 'futsal',
	MINISOCCER = 'mini soccer',
	BASKETBALL = 'basketball'
}

export default class FieldsController {

	/**
	 * @swagger
	 * /fields:
	 *   get:
	 *     security:
	 *       - bearerAuth: []
	 *     tags:
	 *       - Fields
	 *     summary: Get all fields
	 *     responses:
	 *       200:
	 *         description: berhasil get all fields
	 *       400:
	 *         description: Gagal query data
	 *       401:
	 *         description: token tidak valid
	 */
	 public async index({ request, response } : HttpContextContract) {
		try {
			// let fields = await Database
			// 	.from('fields as f')
			// 	.join('venues as v', 'v.id', '=', 'f.venue_id')
			// 	.select('f.*', 'v.name as nama_venue')
			// 	.where('f.venue_id', request.params().venue_id)

			let fields = await Field
				.query()
				.preload('venue')
			if (request.params().venue_id) {
				fields = await Field
					.query()
					.preload('venue')
					.where('venue_id', request.params().venue_id)
			} 
			response.send({message: 'Success', data: fields})
		} catch (err) {
			response.send({message: 'Failed', error: err.message})
		}
	}

	/**
	 * @swagger
	 * /venues/{venue_id}/fields:
	 *   post:
	 *     security:
	 *       - bearerAuth: [admin]
	 *     tags:
	 *       - Fields
	 *     summary: Create field (need 'admin' role)
	 *     parameters:
	 *       - name: venue_id
	 *         in: path
	 *         description: venue id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     requestBody:
	 *       description: name, type
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               name:
	 *                 type: string
	 *               type:
	 *                 type: string
	 *             required: 
	 *               - name
	 *               - type
	 *     responses:
	 *       200:
	 *         description: Berhasil menambah field
	 *       400:
	 *         description: Gagal menambah field
	 *       401:
	 *         description: Token tidak valid/tidak memiliki hak akses
	 */
	 public async store({ bouncer, request, response } : HttpContextContract) {
		const fieldSchema = schema.create({
			name: schema.string({trim: true}, [
				rules.alpha({ allow: ['space'] }),
				rules.minLength(5)
			]),
			type: schema.enum(Object.values(FieldTypes))
		});

		try {
			await bouncer.authorize('createField')
			await request.validate({schema: fieldSchema});
			// let newField = await Database.table('fields').returning('id').insert({
			// 	name: request.input('name'),
			// 	type: request.input('type'),
			// 	venue_id: request.params().venue_id
			// })

			// ORM
			let venue = await Venue.findOrFail(request.params().venue_id);

			// let newField = new Field();
			// newField.name = request.input('name');
			// newField.type = request.input('type');
			// newField.venue_id = request.params().venue_id;
			// await newField.save();
			// console.log(newField)

			// Relationship
			const newField = await venue.related('fields').create({
				name: request.input('name'),
				type: request.input('type'),
			});

			response.created({message: 'berhasil menambahkan data field baru', data: {id: newField.id}});
		} catch (error) {
			let message = error.messages || error.message;

			response.send({message: 'Failed', error: message});
		}
	}

	/**
	 * @swagger
	 * /fields/{id}:
	 *   get:
	 *     security:
	 *       - bearerAuth:
	 *     tags:
	 *       - Fields
	 *     summary: Get field by id
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: field id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: Berhasil get field
	 *       400:
	 *         description: Gagal get field
	 *       401:
	 *         description: Token tidak valid/tidak memiliki hak akses
	 */
	 public async show({ request, response } : HttpContextContract) {
		try {
			// let field = await Database
			// 	.from('fields as f')
			// 	.join('venues as v', 'v.id', '=', 'f.venue_id')
			// 	.select('f.*', 'v.name as nama_venue')
			// 	.where('f.id', request.params().id)
			// 	.first()

			// let field = await Field.query()
			// 	.join('venues as v', 'v.id', '=', 'fields.venue_id')
			// 	.select('fields.*', 'v.name as nama_venue')
			// 	.where('fields.id', request.params().id)
			// 	.first()

			let field = await Field.query().preload('bookings').where('id', request.params().id)
			response.send({message: 'Success', data: field})
		} catch (err) {
			let msg = err.message
			if ('code' in err) {
				msg = err.code
			}
			response.status(500).send({message: 'Failed', error: msg})
			console.log(err)
		}
	}

	/**
	 * @swagger
	 * /fields/{id}:
	 *   put:
	 *     security:
	 *       - bearerAuth: [admin]
	 *     tags:
	 *       - Fields
	 *     summary: Update field (need 'admin' role)
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: field id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     requestBody:
	 *       description: name, type
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               name:
	 *                 type: string
	 *               type:
	 *                 type: string
	 *     responses:
	 *       200:
	 *         description: Berhasil update field
	 *       400:
	 *         description: Gagal update field
	 *       401:
	 *         description: Token tidak valid/tidak memiliki hak akses
	 */
	 public async update({ bouncer, request, response } : HttpContextContract) {
		const fieldSchema = schema.create({
			name: schema.string.optional({trim: true}, [
				rules.alpha({ allow: ['space'] }),
				rules.minLength(5)
			]),
			type: schema.enum.optional(Object.values(FieldTypes))
		});

		try {
			await bouncer.authorize('updateField')
			await request.validate({schema: fieldSchema})
			const name : string = request.input('name')
			const type : "futsal" | "mini soccer" | "basketball" = request.input('type')
			// const affectedRows = await Database
			// 	.from('fields')
			// 	.where('venue_id', request.params().venue_id)
			// 	.where('id', request.params().id)
			// 	.update({name, type})
			let field = await Field.findOrFail(request.params().id);
			field.name = name;
			field.type = type;
			await field.save()
			response.send({message: 'Success', data: field.$original})
		} catch (err) {
			let msg = err.message
			// if ('code' in err) {
			// 	msg = err.code
			// }
			if ('messages' in err) {
				msg = err.messages.errors
			}
			response.status(500).send({message: 'Failed', error: msg})
			console.log(msg)
		}
	}

	/**
	 * @swagger
	 * /fields/{id}:
	 *   delete:
	 *     security:
	 *       - bearerAuth: [admin]
	 *     tags:
	 *       - Fields
	 *     summary: Delete field (need 'admin' role)
	 *     parameters:
	 *       - name: id
	 *         in: path
	 *         description: field id
	 *         required: true
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: Berhasil delete field
	 *       400:
	 *         description: Gagal delete field
	 *       401:
	 *         description: Token tidak valid/tidak memiliki hak akses
	 */
	 public async destroy({ bouncer, request, response } : HttpContextContract) {
		try {
			// const deleted = await Database
			// 	.from('fields')
			// 	.where('venue_id', request.params().venue_id)
			// 	.where('id', request.params().id)
			// 	.delete()
			// const msg = deleted ? 'Success' : 'Tidak ada data yang dihapus'
			await bouncer.authorize('deleteField')
			let field = await Field.findOrFail(request.params().id);
			await field.delete();
			response.send({message: 'Success', data: field})
		} catch (err) {
			let msg = err.message
			// if ('code' in err) {
			// 	msg = err.code
			// }
			response.badRequest({message: msg})
			// console.log(err)
		}
	}

}
