import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'

import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import Mail from '@ioc:Adonis/Addons/Mail'
// import Route from '@ioc:Adonis/Core/Route'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
	/**
	 * @swagger 
	 * /register:
	 *   post:
	 *     tags:
	 *       - Auth
	 *     summary: Register pengguna
	 *     requestBody:
	 *       description: username, email & password
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               name:
	 *                 type: string
	 *               email:
	 *                 type: string
	 *               password:
	 *                 type: string
	 *             required: 
	 *               - name
	 *               - email
	 *               - password
	 *     responses:
	 *       200:
	 *         description: Kode OTP dikirim ke email
	 */
	public async register({ request, response } : HttpContextContract) {
		try {
			const data = await request.validate(UserValidator)

			const newUser = await User.create(data)
			const kodeOTP = Math.floor(100000 + Math.random() * 900000);
			await Database.table('otp_codes').insert({
				otp_code: kodeOTP,
				user_id: newUser.id
			})
			await Mail.send((message) => {
				message
				  .from('adonis.demo@sanberdev.com')
				  .to(newUser.email)
				  .subject('Konfirmasi Kode OTP')
				//   .htmlView('emails/otp_verification', { nama: newUser.name, kodeOTP, link: Route.makeUrl('auth.otp-confirmation') })
				  .htmlView('emails/otp_verification', { nama: newUser.name, kodeOTP })
		   })
			return response.created({message: `Register berhasil, silakan verifikasi melalui email yang Anda daftarkan.`})
		} catch(err) {
			console.log(err)
			let message = err.messages !== undefined ? err.messages : err.message
			return response.badRequest({message})
			// return response.unprocessableEntity({message})
		}
	}

	/**
	 * @swagger
	 * /login:
	 *   post:
	 *     tags:
	 *       - Auth
	 *     summary: Login
	 *     requestBody:
	 *       description: email & password
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               email:
	 *                 type: string
	 *               password:
	 *                 type: string
	 *             required: 
	 *               - email
	 *               - password
	 *     responses:
	 *       200:
	 *         description: Berhasil login
	 *       400:
	 *         description: Gagal login
	 *       401:
	 *         description: Akun belum terverifikasi
	 */
	public async login({ request, response, auth } : HttpContextContract) {
		const email = request.input('email')
		const password = request.input('password')

		try {
			const user = await User.findByOrFail('email', email)
			if (! (user?.isVerified)) {
				return response.status(401).send({message: 'Silakan verifikasi akun Anda terlebih dahulu'})
			}
			const token = await auth.use('api').attempt(email, password, {
				// expiresIn: '10mins'
			})

			return response.ok({ message: 'login sukses', token})
		} catch (err) {
			return response.badRequest({ message: 'Login gagal, periksa kembali email dan password Anda' })
		}
	}

	/**
	 * @swagger
	 * /otp-confirmation:
	 *   post:
	 *     tags:
	 *       - Auth
	 *     summary: Konfirmasi/verifikasi OTP 
	 *     requestBody:
	 *       description: email & kode OTP
	 *       required: true
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               otp_code:
	 *                 type: string
	 *               email:
	 *                 type: string
	 *             required: 
	 *               - otp_code
	 *               - email
	 *     responses:
	 *       200:
	 *         description: Berhasil konfirmasi OTP
	 *       400:
	 *         description: Gagal konfirmasi OTP
	 */
	 public async otpConfirmation({ request, response }: HttpContextContract) {
		const otpSchema = schema.create({
			otp_code: schema.number(),
			email: schema.string()
		})
		// const kodeOTP = request.input('otp_code')
		// const email = request.input('email')
		try {
			const payload = await request.validate({schema: otpSchema})
			const user = await User.findByOrFail('email', payload.email)
			const otp = await Database
				.from('otp_codes')
				.where('otp_code', payload.otp_code)
				.firstOrFail()
			if (user.id == otp.user_id && ! user.isVerified) {
				user.isVerified = true
				await user.save()
				await Database.from('otp_codes')
					.where('otp_code', payload.otp_code)
					.where('user_id', user.id)
					.delete()
				return response.ok({message: 'Berhasil konfirmasi OTP, silakan login'})
			}
			return response.send({message: 'OTP Anda sudah diverifikasi, silakan login'})
		} catch (error) {
			if ('messages' in error) {
				return response.badRequest({message: error.messages})
			}
			if (error.code == 'E_ROW_NOT_FOUND') {
				return response.badRequest({message: 'email/kode OTP tidak ditemukan atau sudah dikonfirmasi'})
			}
			return response.badRequest({ message: error.message })
		}
	}
}
