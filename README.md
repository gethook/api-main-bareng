# API Main Bareng

Aplikasi Main Bareng untuk mempertemukan pemuda-pemuda yang ingin berolahraga tim (futsal/basket/minisoccer) dan booking tempat bersama

### Author 
Akhmad Syarifudin
jl.pesut1no.4@gmail.com

### Link API
[API](https://main-bareng-sanber.herokuapp.com)
[API docs](https://main-bareng-sanber.herokuapp.com/docs)