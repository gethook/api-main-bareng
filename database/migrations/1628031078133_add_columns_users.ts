import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UsersSchema extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      // table.increments('id').primary()
      // table.string('email', 255).unique().notNullable()
      // table.string('password', 180).notNullable()
      table.string('name', 45).nullable().after('id')
      table.string('remember_me_token').nullable().after('password')
      table.string('role', 45).nullable().after('password')

      /**
       * Uses timestampz for PostgreSQL and DATETIME2 for MSSQL
       */
      // table.timestamp('created_at', { useTz: true }).notNullable()
      // table.timestamp('updated_at', { useTz: true }).notNullable()
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('name')
      table.dropColumn('remember_me_token')
      table.dropColumn('role')

    })
  }
}
